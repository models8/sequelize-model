# CLONE TEMPLATE: 

- `arbitrage-model` version 

```bash
git clone --single-branch --branch arbitrage-model git@gitlab.com:models8/sequelize-model.git
```

- `fake-model` version 

```bash
git clone --single-branch --branch fake-model git@gitlab.com:models8/sequelize-model.git
```

# UPDATE DEPENDENCIES: 

https://www.npmjs.com/package/npm-check-updates

```bash
npx npm-check-updates -i
```
